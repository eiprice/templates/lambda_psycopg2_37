FROM python:3.7

WORKDIR /app

COPY . /app

RUN pip install python-dateutil pytz

CMD ["python", "/app/lambda.py" ]
