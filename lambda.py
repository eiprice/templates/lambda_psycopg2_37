#!/usr/bin/env python

import psycopg2
import os

try:
    config = [os.environ['DB_HOST'], os.environ['DB_DATABASE'],
              os.environ['DB_USER'], os.environ['DB_PASS'], os.environ['DB_PORT']]
except KeyError:
    print("Existem váriaveis de ambiente não definidas")
    exit(1)

query = """
SELECT 1 from {schema}.table where {id}={id}
"""


def exec_db(cur, conn):
    print("Inserting data on {}:{}".format(name, id))
    cur.execute(query.format(schema=name, id=id))
    conn.commit()


def main(event=None, context=None):
    conn = psycopg2.connect(
        "host={} dbname={} user={} password={} port={}".format(*config))
    cur = conn.cursor()

    exec_db(cur, conn)
    conn.close()


if __name__ == "__main__":
  event = {'hi': 'world'}
  main(event, {})
